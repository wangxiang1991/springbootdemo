package com.example.demo.access;

import com.example.demo.DemoApplicationTests;
import com.example.demo.entity.access.Menu;
import com.example.demo.service.access.IMenuService;
import com.example.demo.utils.MessageBody;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by jake on 19-4-14.
 */
public class TestMenu extends DemoApplicationTests {

    @Autowired
    private IMenuService menuService;

    @Before
    public void setUp() throws Exception {
        menuService.deleteAll();
    }

    @Test
    public void testInserMenu() {
        for (int i = 1; i < 4; i++) {
            Menu menu = new Menu();
            menu.setName("测试1级菜单:" + i);
            menu.setUrl("测试url/" + i);
            menu.setCreatedate(new Date());
            menu.setUpdatedate(new Date());
            menu.setCreateuserid(i);
            menu.setIcon("icon" + i);
            menu.setIsdelete(0);
            menu.setIsshow(1);
            menu.setParentid("0");
            menu.setParentids("0,");
            menu.setSort(i);
            MessageBody result = menuService.insertMenu(menu);
            for (int j = 1; j < 4; j++) {
                String parentid = menuService.findMenuByName(menu.getName()).getId() + "";
                Menu childMenu = new Menu();
                childMenu.setName("测试1级菜单" + i + "的子菜单:" + j);
                childMenu.setUrl("测试" + i + "级菜单的子菜单url/" + j);
                childMenu.setCreatedate(new Date());
                childMenu.setUpdatedate(new Date());
                childMenu.setCreateuserid(j);
                childMenu.setIcon("icon" + j);
                childMenu.setIsdelete(0);
                childMenu.setIsshow(1);
                childMenu.setParentid(parentid);
                childMenu.setParentids(parentid + ",");
                childMenu.setSort(j);
                MessageBody child =menuService.insertMenu(childMenu);
                for(int k=1;k<4;k++){
                    String parentid2 = menuService.findMenuByName(childMenu.getName()).getId() + "";
                    Menu childMenu2 = new Menu();
                    childMenu2.setName("测试2级菜单" + j + "的子菜单:" + k);
                    childMenu2.setUrl("测试" + j + "级菜单的子菜单url/" + k);
                    childMenu2.setCreatedate(new Date());
                    childMenu2.setUpdatedate(new Date());
                    childMenu2.setCreateuserid(k);
                    childMenu2.setIcon("icon" + k);
                    childMenu2.setIsdelete(0);
                    childMenu2.setIsshow(1);
                    childMenu2.setParentid(parentid2);
                    childMenu2.setParentids(parentid2 + ",");
                    childMenu2.setSort(j);
                    menuService.insertMenu(childMenu2);
                }
            }
        }

        List<Menu> menus = menuService.listMenusAsTree();
        int a = 1;
    }
}
