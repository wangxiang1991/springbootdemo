package com.example.demo.access;

import com.example.demo.DemoApplicationTests;
import com.example.demo.service.access.IUserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jake on 19-4-14.
 */
public class TestUser extends DemoApplicationTests {
    @Autowired
    private WebApplicationContext context;
    private MockMvc mvc;

    @Autowired
    private IUserService userService;

    @Before
    public void setUp() throws Exception {
        mvc = MockMvcBuilders.webAppContextSetup(context).build();//建议使用这种
        userService.deleteUserByUsername("ceshi");
    }

    @Test
    public void contextLoads() {
        String[] a = new String[]{"1", "2", "3"};
    }

    @Test
    public void testRegister() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String result = "";
        Map<String, String> registerSuccessMap = new HashMap<>();
        MvcResult mvcResult;
        // 第一个测试会通过
        mvcResult = mvc.perform(MockMvcRequestBuilders.get("/register")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .param("status", "1")
                .param("username", "ceshi")
                .param("nickname", "ceshi_nick")
                .param("mobilephone", "13211112222")
                .param("email", "13211112222@qq.com")
                .param("address", "测试地址")
                .param("password", "111")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
        result = mvcResult.getResponse().getContentAsString();
        registerSuccessMap = mapper.readValue(result, Map.class);
        // 第一次注册成功
        Assert.assertEquals(registerSuccessMap.get("result"), "success");

        mvcResult = mvc.perform(MockMvcRequestBuilders.get("/register")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .param("status", "1")
                .param("username", "ceshi")
                .param("nickname", "ceshi_nick")
                .param("mobilephone", "13211112222")
                .param("email", "13211112222@qq.com")
                .param("address", "测试地址")
                .param("password", "111")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
        result = mvcResult.getResponse().getContentAsString();
        registerSuccessMap = mapper.readValue(result, Map.class);
        // 第二次注册 注册失败 用户名称重复
        Assert.assertEquals(registerSuccessMap.get("result"), "fail");

        mvcResult = mvc.perform(MockMvcRequestBuilders.get("/register")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .param("status", "1")
                .param("username", "ceshi1")
                .param("nickname", "ceshi_nick")
                .param("mobilephone", "13211112222")
                .param("email", "13211112222@qq.com")
                .param("address", "测试地址")
                .param("password", "111")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
        result = mvcResult.getResponse().getContentAsString();
        registerSuccessMap = mapper.readValue(result, Map.class);
        // 第二次注册 注册失败 用户名称重复
        Assert.assertEquals(registerSuccessMap.get("result"), "fail");

        mvcResult = mvc.perform(MockMvcRequestBuilders.get("/register")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .param("status", "1")
                .param("username", "ceshi1")
                .param("nickname", "ceshi_nick1")
                .param("mobilephone", "13211112222")
                .param("email", "13211112222@qq.com")
                .param("address", "测试地址")
                .param("password", "111")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
        result = mvcResult.getResponse().getContentAsString();
        registerSuccessMap = mapper.readValue(result, Map.class);
        // 第二次注册 注册失败 用户名称重复
        Assert.assertEquals(registerSuccessMap.get("result"), "fail");

        mvcResult = mvc.perform(MockMvcRequestBuilders.get("/register")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .param("status", "1")
                .param("username", "ceshi1")
                .param("nickname", "ceshi_nick1")
                .param("mobilephone", "132111122221")
                .param("email", "13211112222@qq.com")
                .param("address", "测试地址")
                .param("password", "111")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
        result = mvcResult.getResponse().getContentAsString();
        registerSuccessMap = mapper.readValue(result, Map.class);
        // 第二次注册 注册失败 用户名称重复
        Assert.assertEquals(registerSuccessMap.get("result"), "fail");

    }
}
