//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.entity.tree;

import java.util.ArrayList;
import java.util.List;

public class TreeNode {
    private String id;
    private String icon;
    private Boolean genChildren;
    private String name;
    private String href;
    private List<TreeNode> children = null;
    private Boolean open;
    private Boolean isParent;

    public Boolean getIsParent() {
        return isParent;
    }

    public void setIsParent(Boolean isParent) {
        this.isParent = isParent;
    }

    public void addChildren(TreeNode tn) {
        if(this.children == null) {
            this.children = new ArrayList();
        }

        this.children.add(tn);
    }

    public TreeNode() {
    }

    public TreeNode(String id) {
        this.id = id;
    }

    public TreeNode(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public TreeNode(String id, String name, String href) {
        this.id = id;
        this.name = name;
        this.href = href;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List getChildren() {
        return this.children;
    }

    public void setChildren(List<TreeNode> children) {
        this.children = children;
    }

    public Boolean isOpen() {
        return this.open;
    }

    public void setOpen(Boolean open) {
        this.open = open;
    }

    public Boolean getGenChildren() {
        return this.genChildren;
    }

    public void setGenChildren(Boolean genChildren) {
        this.genChildren = genChildren;
        if(genChildren.booleanValue()) {
            this.children = new ArrayList();
        }

    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIcon() {
        return this.icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getHref() {
        return this.href;
    }

    public void setHref(String href) {
        this.href = href;
    }
}
