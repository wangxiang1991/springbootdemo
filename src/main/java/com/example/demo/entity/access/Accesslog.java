package com.example.demo.entity.access;

import java.util.Date;

/**
 * 这个是 访问记录表 accesslog 映射对象
 */
public class Accesslog {
    private Integer id;
    // 访问菜单名称
    private String title;
    // 访问类型
    private Integer accesstype;
    // uri
    private String uri;
    private String servletname;
    private Integer servletport;
    private String method;
    private String username;
    private Date accesstime;
    private String useragent;
    private String operatingsystem;
    private String browser;
    private Integer custmillisecond;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getAccesstype() {
        return accesstype;
    }

    public void setAccesstype(Integer accesstype) {
        this.accesstype = accesstype;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getServletname() {
        return servletname;
    }

    public void setServletname(String servletname) {
        this.servletname = servletname;
    }

    public Integer getServletport() {
        return servletport;
    }

    public void setServletport(Integer servletport) {
        this.servletport = servletport;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getAccesstime() {
        return accesstime;
    }

    public void setAccesstime(Date accesstime) {
        this.accesstime = accesstime;
    }

    public String getUseragent() {
        return useragent;
    }

    public void setUseragent(String useragent) {
        this.useragent = useragent;
    }

    public String getOperatingsystem() {
        return operatingsystem;
    }

    public void setOperatingsystem(String operatingsystem) {
        this.operatingsystem = operatingsystem;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public Integer getCustmillisecond() {
        return custmillisecond;
    }

    public void setCustmillisecond(Integer custmillisecond) {
        this.custmillisecond = custmillisecond;
    }

    @Override
    public String toString() {
        return "Accesslog{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", accesstype=" + accesstype +
                ", uri='" + uri + '\'' +
                ", servletname='" + servletname + '\'' +
                ", servletport=" + servletport +
                ", method='" + method + '\'' +
                ", username='" + username + '\'' +
                ", accesstime=" + accesstime +
                ", useragent='" + useragent + '\'' +
                ", operatingsystem='" + operatingsystem + '\'' +
                ", browser='" + browser + '\'' +
                ", custmillisecond=" + custmillisecond +
                '}';
    }
}
