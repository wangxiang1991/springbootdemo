package com.example.demo.entity.access;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jake on 19-4-14.
 */
public class Menu {
    private Integer id;
    private String parentid;
    private String parentids;
    private String name;
    private Integer createuserid;
    private Integer updateuserid;
    private Integer sort;
    private String url;
    private String icon;
    private Integer isshow;
    private String permission;
    private Date createdate;
    private Date updatedate;
    private Integer isdelete;
    private String remarks;

    public List<Menu> childMenus=new ArrayList<>();

    public List<Menu> getChildMenus() {
        return childMenus;
    }

    public void setChildMenus(List<Menu> childMenus) {
        this.childMenus = childMenus;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getParentid() {
        return parentid;
    }

    public void setParentid(String parentid) {
        this.parentid = parentid;
    }

    public String getParentids() {
        return parentids;
    }

    public void setParentids(String parentids) {
        this.parentids = parentids;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCreateuserid() {
        return createuserid;
    }

    public void setCreateuserid(Integer createuserid) {
        this.createuserid = createuserid;
    }

    public Integer getUpdateuserid() {
        return updateuserid;
    }

    public void setUpdateuserid(Integer updateuserid) {
        this.updateuserid = updateuserid;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getIsshow() {
        return isshow;
    }

    public void setIsshow(Integer isshow) {
        this.isshow = isshow;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public Date getUpdatedate() {
        return updatedate;
    }

    public void setUpdatedate(Date updatedate) {
        this.updatedate = updatedate;
    }

    public Integer getIsdelete() {
        return isdelete;
    }

    public void setIsdelete(Integer isdelete) {
        this.isdelete = isdelete;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "id=" + id +
                ", parentid='" + parentid + '\'' +
                ", parentids='" + parentids + '\'' +
                ", name='" + name + '\'' +
                ", createuserid=" + createuserid +
                ", updateuserid=" + updateuserid +
                ", sort=" + sort +
                ", url='" + url + '\'' +
                ", icon='" + icon + '\'' +
                ", isshow=" + isshow +
                ", permission='" + permission + '\'' +
                ", createdate=" + createdate +
                ", updatedate=" + updatedate +
                ", isdelete=" + isdelete +
                ", remarks='" + remarks + '\'' +
                ", childMenus=" + childMenus +
                '}';
    }
}
