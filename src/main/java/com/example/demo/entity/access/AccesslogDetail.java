package com.example.demo.entity.access;

public class AccesslogDetail {
    private Integer id;
    private Integer accessid;
    private String parametername;
    private String parametervalue;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAccessid() {
        return accessid;
    }

    public void setAccessid(Integer accessid) {
        this.accessid = accessid;
    }

    public String getParametername() {
        return parametername;
    }

    public void setParametername(String parametername) {
        this.parametername = parametername;
    }

    public String getParametervalue() {
        return parametervalue;
    }

    public void setParametervalue(String parametervalue) {
        this.parametervalue = parametervalue;
    }

    @Override
    public String toString() {
        return "AccesslogDetail{" +
                "id=" + id +
                ", accessid=" + accessid +
                ", parametername='" + parametername + '\'' +
                ", parametervalue='" + parametervalue + '\'' +
                '}';
    }
}
