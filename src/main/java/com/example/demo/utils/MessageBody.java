package com.example.demo.utils;

import java.util.HashMap;
import java.util.Map;

public class MessageBody {

    private Integer code;

    private boolean isSuccess;

    private String msg;

    private Map data=new HashMap();

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Map getData() {
        return data;
    }

    public void setData(Map data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "MessageBody{" +
                "code='" + code + '\'' +
                ", isSuccess=" + isSuccess +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
