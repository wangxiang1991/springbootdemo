package com.example.demo.utils;

public enum MessageCodeEnum {
    /*
    200 OK [GET] 服务器成功返回用户请求的数据，该操作是幂等的（Idempotent）

    201 CREATED [POST/PUT/PATCH] 用户新建或修改数据成功

    202 ACCEPTED [*] 表示一个请求已经进入后台排队（异步任务）

    204 NO CONTENT [DELETE] 用户删除数据成功

    400 INVALID REQUEST [POST/PUT/PATCH] 用户发出的请求有错误，服务器没有进行新建或修改数据的操作，该操作是幂等的

    401 UNAUTHORIZED [*] 表示用户没有权限（令牌、用户名、密码错误）

    403 FORBIDDEN [*] 表示用户得到授权（与401错误相对），但是访问是被禁止的

    404 NOT FOUND [*] 用户发出的请求针对的是不存在的记录，服务器没有进行操作，该操作是幂等的

    406 NOT ACCEPTABLE [GET] 用户请求的格式不可得（比如用户请求JSON格式，但是只有XML格式）

    410 GONE [GET] 用户请求的资源被永久删除，且不会再得到的

    422 UNPROCESABLE ENTITY [POST/PUT/PATCH] 当创建一个对象时，发生一个验证错误

    500 INTERNAL SERVER ERROR [*] 服务器发生错误，用户将无法判断发出的请求是否成功
    */
    CODE_200(200, "OK:服务器成功返回用户请求的数据"),
    CODE_201(201, "CREATED:用户新建或修改数据成功"),
    CODE_202(202, "ACCEPTED:请求已经进入后台排队（异步任务）"),
    CODE_204(204, "DELETE:用户删除数据成功"),
    CODE_400(400, "INVALID REQUEST:用户发出的请求有错误，服务器没有进行新建或修改数据的操作"),
    CODE_401(401, "UNAUTHORIZED:表示用户没有权限（令牌、用户名、密码错误）"),
    CODE_403(403, "FORBIDDEN:表示用户得到授权（与401错误相对），但是访问是被禁止的"),
    CODE_404(404, "NOT FOUND:用户发出的请求针对的是不存在的记录，服务器没有进行操作"),
    CODE_406(406, "NOT ACCEPTABLE:用户请求的格式不可得（比如用户请求JSON格式，但是只有XML格式）"),
    CODE_410(410, "GONE:用户请求的资源被永久删除，且不会再得到的"),
    CODE_422(422, "UNPROCESABLE ENTITY:当创建一个对象时，发生一个验证错误"),
    CODE_500(500, "INTERNAL SERVER ERROR:服务器发生错误，无法判断发出的请求是否成功"),;

    private Integer code;
    private String msg;

    MessageCodeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * 根据msg获取code
     *
     * @param msg
     * @return
     */
    public Integer getCodeByMsg(String msg) {
        MessageCodeEnum[] enums = MessageCodeEnum.values();
        for (MessageCodeEnum e : enums) {
            if (e.getMsg().equals(msg)) {
                return e.getCode();
            }
        }
        return -1;
    }

    /**
     * 根据code获取msg
     *
     * @param code
     * @return
     */
    public String getMsgByCode(Integer code) {
        MessageCodeEnum[] enums = MessageCodeEnum.values();
        for (MessageCodeEnum e : enums) {
            if (e.getCode().equals(code)) {
                return e.getMsg();
            }
        }
        return "-1";
    }
}
