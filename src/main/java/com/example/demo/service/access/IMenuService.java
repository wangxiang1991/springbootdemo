package com.example.demo.service.access;

import com.example.demo.entity.access.Menu;
import com.example.demo.entity.tree.TreeNode;
import com.example.demo.utils.MessageBody;

import java.util.List;
import java.util.Map;

/**
 * Created by jake on 19-4-14.
 */
public interface IMenuService {
    /**
     * 查找所有的菜单 以树的形式返回结果
     */
    public List<Menu> listMenusAsTree();

    /**
     * 新增菜单
     * @param menu
     */
    public MessageBody insertMenu(Menu menu);

    /**
     * 更新菜单
     * @param menu
     * @return
     */
    public MessageBody updateMenu(Menu menu);

    public MessageBody updateMenu(Integer id);

    /**
     * 从allMenus 查找parentMenu所有的子菜单
     * @param parentMenu
     * @param allMenus
     * @return
     */
    public List<Menu> listAllChildMenu(Menu parentMenu, List<Menu> allMenus);

    /**
     * 从allMenus 查找所有一级菜单
     * @param allMenus
     * @return
     */
    public List<Menu> listAllChildMenu(List<Menu> allMenus);

    /**
     * 根据菜单名称查找菜单
     * @param name
     * @return
     */
    public Menu findMenuByName(String name);

    /**
     * 删除所有的菜单
     */
    public void deleteAll();

    public List<Menu> listChilds(String parentid);

    public List<Menu> listAllMenu();

    public Menu findMenuById(int id);

    public List<Menu> listMenuAsTree(int id);

    /**
     * 根据id删除菜单
     * @param id
     * @return
     */
    public MessageBody deleteByid(Integer id);


    public TreeNode listMenuAsTreeNode(Integer parentid);
}
