package com.example.demo.service.access;

import com.example.demo.entity.access.Accesslog;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public interface IAccesslogService {

    /**
     * 保存访问日志详情
     * @param log
     * @param parameterMap
     */
    public void insertAccesslog(Accesslog log, Map<String,String[]> parameterMap);


    public void insertAccesslog(HttpServletRequest request);
}
