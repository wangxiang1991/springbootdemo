package com.example.demo.service.access.impl;

import com.example.demo.dao.access.MenuMapper;
import com.example.demo.entity.access.Menu;
import com.example.demo.entity.tree.TreeNode;
import com.example.demo.service.access.IMenuService;
import com.example.demo.utils.MessageBody;
import com.example.demo.utils.MessageCodeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jake on 19-4-14.
 */
@Service
public class MenuServiceImpl implements IMenuService {
    private transient static final Logger LOGGER = LoggerFactory.getLogger(MenuServiceImpl.class);

    @Autowired
    private MenuMapper menuMapper;

    @Override
    public List<Menu> listMenusAsTree() {
        List<Menu> allMenus = menuMapper.listAllMenu();
        List<Menu> topMenus = this.listAllChildMenu(allMenus);
        for (Menu menu : topMenus) {
            this.setChildMenus(menu, allMenus);
        }
        return topMenus;
    }

    public void setChildMenus(Menu parentMenu, List<Menu> allMenus) {
        List<Menu> childMenus = this.listAllChildMenu(parentMenu, allMenus);
        if (childMenus != null && childMenus.size() > 0) {
            parentMenu.setChildMenus(childMenus);
            for (Menu child : childMenus) {
                this.setChildMenus(child, allMenus);
            }
        } else {
            return;
        }
    }

    @Override
    public List<Menu> listAllChildMenu(Menu parentMenu, List<Menu> allMenus) {
        List<Menu> childMenus = new ArrayList<>();
        LOGGER.trace("parentMenu={}", parentMenu);
        for (Menu menu : allMenus) {
            if ((parentMenu.getId() + "").equals(menu.getParentid())) {
                LOGGER.trace("添加menu{}为子菜单", menu);
                childMenus.add(menu);
            }
        }
        return childMenus;
    }

    @Override
    public List<Menu> listAllChildMenu(List<Menu> allMenus) {
        List<Menu> topMenus = new ArrayList<>();
        for (Menu menu : allMenus) {
            if (menu.getParentid() == null ||
                    menu.getParentid().isEmpty() ||
                    menu.getParentid().equals("0")) {
                topMenus.add(menu);
            }
        }
        return topMenus;
    }

    @Override
    public MessageBody insertMenu(Menu menu) {
        MessageBody body = new MessageBody();
        body.setSuccess(true);
        if (menu == null) {
            body.setSuccess(false);
            body.setCode(MessageCodeEnum.CODE_400.getCode());
            body.setMsg("菜单为空");
        }

        if (menu.getName() == null || menu.getName().isEmpty()) {
            body.setSuccess(false);
            body.setCode(MessageCodeEnum.CODE_400.getCode());
            body.setMsg("菜单名称为空");
        } else {
            Menu tmpMenu = menuMapper.findMenuByName(menu.getName());
            if (tmpMenu != null) {
                body.setSuccess(false);
                body.setCode(MessageCodeEnum.CODE_400.getCode());
                body.setMsg("菜单名称重复");
            }
        }

        if (menu.getUrl() == null || menu.getUrl().isEmpty()) {
            boolean isThirdLevelMenu = false;
            String parentids = menu.getParentids();
            if (parentids != null && !parentids.isEmpty()) {
                String[] menuLevels = parentids.split(",");
                if (menuLevels != null && menuLevels.length > 2) {
                    isThirdLevelMenu = true;
                }
            }

            if (isThirdLevelMenu) {
                body.setSuccess(false);
                body.setCode(MessageCodeEnum.CODE_400.getCode());
                body.setMsg("菜单url为空");
            }
        }


        if (body.isSuccess()) {
            body.setSuccess(true);
            body.setCode(MessageCodeEnum.CODE_200.getCode());
            body.setMsg("新增菜单成功");
            menuMapper.insertMenu(menu);
        }

        return body;
    }

    @Override
    public MessageBody updateMenu(Menu menu) {
        MessageBody body = new MessageBody();
        body.setSuccess(true);
        if (menu == null) {
            body.setSuccess(false);
            body.setCode(MessageCodeEnum.CODE_400.getCode());
            body.setMsg("菜单为空");
        } else if (menu.getId() == null || menu.getId() == 0) {
            body.setSuccess(false);
            body.setCode(MessageCodeEnum.CODE_400.getCode());
            body.setMsg("菜单id为空");
        }

        if (menu.getName() == null || menu.getName().isEmpty()) {
            body.setSuccess(false);
            body.setCode(MessageCodeEnum.CODE_400.getCode());
            body.setMsg("菜单名称为空");
        } else {
            Menu tmpMenu = menuMapper.findMenuByName(menu.getName());
            if (tmpMenu != null && tmpMenu.getId().intValue() != menu.getId().intValue()) {
                body.setSuccess(false);
                body.setCode(MessageCodeEnum.CODE_400.getCode());
                body.setMsg("菜单名称重复");
            }
        }

        if (menu.getUrl() == null || menu.getUrl().isEmpty()) {
            boolean isThirdLevelMenu = false;
            String parentids = menu.getParentids();
            if (parentids != null && !parentids.isEmpty()) {
                String[] menuLevels = parentids.split(",");
                if (menuLevels != null && menuLevels.length > 2) {
                    isThirdLevelMenu = true;
                }
            }

            if (isThirdLevelMenu) {
                body.setSuccess(false);
                body.setCode(MessageCodeEnum.CODE_400.getCode());
                body.setMsg("菜单url为空");
            }
        }


        if (body.isSuccess()) {
            body.setSuccess(true);
            body.setCode(MessageCodeEnum.CODE_200.getCode());
            body.setMsg("更新菜单成功");
            menuMapper.updateMenu(menu);
        }
        LOGGER.info("更新菜单:{}的结果为:{}", menu, body);

        return body;
    }

    @Override
    public MessageBody updateMenu(Integer id) {
        Menu menu = menuMapper.findMenuById(id);
        return this.updateMenu(menu);
    }

    @Override
    public Menu findMenuByName(String name) {
        return menuMapper.findMenuByName(name);
    }

    @Override
    public void deleteAll() {
        menuMapper.deleteAll();
    }

    @Override
    public List<Menu> listChilds(String parentid) {
        return menuMapper.findChildMenus(parentid);
    }

    @Override
    public List<Menu> listAllMenu() {
        return menuMapper.listAllMenu();
    }

    @Override
    public Menu findMenuById(int id) {
        return menuMapper.findMenuById(id);
    }

    @Override
    public List<Menu> listMenuAsTree(int id) {
        List<Menu> menus = menuMapper.findChildMenus(id + "");
        List<Menu> allMenus = this.listAllMenu();
        for (Menu menu : menus) {
            this.setChildMenus(menu, allMenus);
        }
        return menus;
    }

    @Override
    public MessageBody deleteByid(Integer id) {
        MessageBody body = new MessageBody();
        body.setSuccess(true);

        if (id == null || id == 0) {
            body.setSuccess(false);
            body.setCode(MessageCodeEnum.CODE_400.getCode());
            body.setMsg("删除菜单id不能为空");
        }

        Menu menu = menuMapper.findMenuById(id);
        if (menu == null) {
            body.setSuccess(false);
            body.setCode(MessageCodeEnum.CODE_400.getCode());
            body.setMsg("菜单不存在");
        }

        if (menu != null) {
            List<Menu> childs = menuMapper.findChildMenus(menu.getId().toString());
            if (childs != null && childs.size() > 0) {
                body.setSuccess(false);
                body.setCode(MessageCodeEnum.CODE_400.getCode());
                body.setMsg("不能直接删除父级菜单");
            }
        }

        if (body.isSuccess()) {
            menu.setIsdelete(1);
            menuMapper.updateMenu(menu);
            body.setSuccess(true);
            body.setCode(MessageCodeEnum.CODE_200.getCode());
            body.setMsg("删除成功");
        }
        LOGGER.info("删除菜单{}的结果为{}", menu, body);
        return body;
    }

    @Override
    public TreeNode listMenuAsTreeNode(Integer parentid) {
        List<Menu> allMenus = menuMapper.listAllMenu();
        List<Menu> menus = menuMapper.findChildMenus(parentid + "");
        Menu parentMenu = menuMapper.findMenuById(parentid);
        TreeNode parentNode = new TreeNode();
        for (Menu menu : menus) {
            this.setChildMenus(menu, allMenus);
        }
        return parentNode;
    }


}
