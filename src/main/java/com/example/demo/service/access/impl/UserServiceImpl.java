package com.example.demo.service.access.impl;

import com.example.demo.dao.access.UserMapper;
import com.example.demo.entity.access.User;
import com.example.demo.service.access.IUserService;
import com.example.demo.utils.MessageBody;
import com.example.demo.utils.MessageCodeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Service
public class UserServiceImpl implements IUserService {
    private transient static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserMapper userMapper;

    @Override
    public Set<String> findRoles(String username) {
        Set<String> set = new HashSet<>();
        set.add("测试角色1");
        set.add("测试角色2");
        set.add("测试角色3");
        set.add("测试角色4");
        return set;
    }

    @Override
    public Set<String> findPermissions(String username) {
        Set<String> set = new HashSet<>();
        set.add("权限权限1");
        set.add("权限权限2");
        set.add("权限权限3");
        set.add("权限权限4");
        return set;
    }

    @Override
    public User findByUsername(String username) {
        User user = userMapper.findByUsername(username);
        return user;
    }

    @Override
    public MessageBody registerUser(User user) {
        MessageBody body = new MessageBody();
        body.setSuccess(true);
        if (user == null) {
            body.setSuccess(false);
            body.setCode(MessageCodeEnum.CODE_400.getCode());
            body.setMsg("用户信息为空");
        }
        if (user.getUsername() != null && !user.getUsername().isEmpty()) {
            User tmpUser = userMapper.findByUsername(user.getUsername());
            if (tmpUser != null) {
                body.setSuccess(false);
                body.setCode(MessageCodeEnum.CODE_400.getCode());
                body.setMsg("用户名重复");
            }
        }
        if (user.getNickname() != null && !user.getNickname().isEmpty()) {
            User tmpUser = userMapper.findByNickname(user.getNickname());
            if (tmpUser != null) {
                body.setSuccess(false);
                body.setCode(MessageCodeEnum.CODE_400.getCode());
                body.setMsg("昵称重复");
            }
        }
        if (user.getMobilephone() != null && !user.getMobilephone().isEmpty()) {
            User tmpUser = userMapper.findByMobilphone(user.getMobilephone());
            if (tmpUser != null) {
                body.setSuccess(false);
                body.setCode(MessageCodeEnum.CODE_400.getCode());
                body.setMsg("手机号重复");
            }
        }
        if (user.getEmail() != null && !user.getEmail().isEmpty()) {
            User tmpUser = userMapper.findByEmail(user.getEmail());
            if (tmpUser != null) {
                body.setSuccess(false);
                body.setCode(MessageCodeEnum.CODE_400.getCode());
                body.setMsg("邮箱重复");
            }
        }
        if (body.isSuccess()) {
            body.setSuccess(true);
            body.setCode(MessageCodeEnum.CODE_200.getCode());
            body.setMsg("注册成功");
            userMapper.insertUser(user);
        }
        LOGGER.info("新增用户:{}的结果为:{}", user, body);
        return body;
    }

    @Override
    public void deleteUserByUsername(String username) {
        LOGGER.info("开始根据用户名:{},删除用户", username);
        this.userMapper.deleteUserByUsername(username);
    }

    @Override
    public MessageBody updateUser(User user) {
        LOGGER.info("开始更新用户user:{}", user);
        MessageBody body = new MessageBody();
        body.setSuccess(true);
        if (user == null) {
            body.setSuccess(false);
            body.setCode(MessageCodeEnum.CODE_400.getCode());
            body.setMsg("用户信息为空");
        } else if (user.getId() == null || user.getId() == 0) {
            body.setSuccess(false);
            body.setMsg("用户id为空");
        }

        if (user.getUsername() != null && !user.getUsername().isEmpty()) {
            User tmpUser = userMapper.findByUsername(user.getUsername());
            if (tmpUser != null && user.getId().equals(tmpUser.getId())) {
                body.setSuccess(false);
                body.setCode(MessageCodeEnum.CODE_400.getCode());
                body.setMsg("用户名重复");
            }
        }
        if (user.getNickname() != null && !user.getNickname().isEmpty()) {
            User tmpUser = userMapper.findByNickname(user.getNickname());
            if (tmpUser != null && user.getId().equals(tmpUser.getId())) {
                body.setSuccess(false);
                body.setCode(MessageCodeEnum.CODE_400.getCode());
                body.setMsg("昵称重复");
            }
        }
        if (user.getMobilephone() != null && !user.getMobilephone().isEmpty()) {
            User tmpUser = userMapper.findByMobilphone(user.getMobilephone());
            if (tmpUser != null && user.getId().equals(tmpUser.getId())) {
                body.setSuccess(false);
                body.setCode(MessageCodeEnum.CODE_400.getCode());
                body.setMsg("手机号重复");
            }
        }
        if (user.getEmail() != null && !user.getEmail().isEmpty()) {
            User tmpUser = userMapper.findByEmail(user.getEmail());
            if (tmpUser != null && user.getId().equals(tmpUser.getId())) {
                body.setSuccess(false);
                body.setCode(MessageCodeEnum.CODE_400.getCode());
                body.setMsg("邮箱重复");
            }
        }
        if (body.isSuccess()) {
            body.setSuccess(true);
            body.setMsg("更新成功");
            body.setCode(MessageCodeEnum.CODE_200.getCode());
            userMapper.updateUser(user);
        }
        LOGGER.info("更新用户:{}的结果为:{}", user, body);
        return body;
    }
}
