package com.example.demo.service.access;

import com.example.demo.entity.access.User;
import com.example.demo.utils.MessageBody;

import java.util.Map;
import java.util.Set;

public interface IUserService {
    /**
     * 根据用户名查找角色
     * @param username
     * @return
     */
    public Set<String> findRoles(String username);

    /**
     * 根据用户名查找权限
     * @param username
     * @return
     */
    public Set<String> findPermissions(String username);

    /**
     * 根据用户名查找用户
     * @param username
     * @return
     */
    public User findByUsername(String username);

    /**
     * 验证用户注册信息的唯一性
     */
    public MessageBody registerUser(User user);

    /**
     * 根据用户名删除用户
     * @param username
     */
    public void deleteUserByUsername(String username);

    /**
     * 更新用户
     * @param user
     */
    public MessageBody updateUser(User user);
}
