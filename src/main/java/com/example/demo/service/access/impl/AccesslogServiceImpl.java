package com.example.demo.service.access.impl;

import com.example.demo.dao.access.AccesslogDetailMapper;
import com.example.demo.dao.access.AccesslogMapper;
import com.example.demo.entity.access.Accesslog;
import com.example.demo.entity.access.AccesslogDetail;
import com.example.demo.service.access.IAccesslogService;
import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.UserAgent;
import eu.bitwalker.useragentutils.Version;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.Map;

@Service
public class AccesslogServiceImpl implements IAccesslogService {
    private transient static final Logger LOGGER = LoggerFactory.getLogger(AccesslogServiceImpl.class);
    @Autowired
    private AccesslogMapper accesslogMapper;

    @Autowired
    private AccesslogDetailMapper accesslogDetailMapper;


    @Override
    public void insertAccesslog(Accesslog log, Map<String, String[]> parameterMap) {
        LOGGER.info("开始保存访问记录及请求参数,参数为：{}", log);
        int accessid = this.accesslogMapper.inserAccesslog(log);
        LOGGER.info("保存访问日志主记录表，id为{},开始保存参数详情", accessid);
        if (parameterMap != null && !parameterMap.isEmpty()) {
            for (Map.Entry<String, String[]> entry : parameterMap.entrySet()) {
                AccesslogDetail detail = new AccesslogDetail();
                detail.setAccessid(accessid);
                detail.setParametername(entry.getKey());
                String[] values = entry.getValue();
                if (values != null && values.length > 0) {
                    detail.setParametervalue(StringUtils.join(values));
                }
                LOGGER.info("开始保存访问日志请求参数表，detail：{}", detail);
                accesslogDetailMapper.insertAccesslogDetail(detail);
            }
        }
    }

    @Override
    public void insertAccesslog(HttpServletRequest request) {
        Accesslog accesslog = new Accesslog();
        accesslog.setTitle("测试");
        accesslog.setAccesstype(1);
        String uri = request.getRequestURI();
        accesslog.setUri(uri);
        String servletName = request.getServerName();
        int servletPort = request.getServerPort();
        accesslog.setServletname(servletName);
        accesslog.setServletport(servletPort);
        String method = request.getMethod();
        accesslog.setMethod(method);
        Subject subject = SecurityUtils.getSubject();
        if (subject != null && subject.getPrincipal() != null) {
            accesslog.setUsername(subject.getPrincipal().toString());
        }
        accesslog.setAccesstime(Calendar.getInstance().getTime());
        accesslog.setUseragent(request.getHeader("User-Agent"));
        //获取浏览器信息
        UserAgent agent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
        if (agent != null) {
            accesslog.setOperatingsystem(agent.getOperatingSystem().getName());
            Browser browser = agent.getBrowser();
            //获取浏览器版本号
            if (browser != null) {
                Version version = browser.getVersion(request.getHeader("User-Agent"));
                if (version != null) {
                    accesslog.setBrowser(browser.getName() + "   " + version.getVersion());
                }
            }
        }
        accesslog.setCustmillisecond(10);
        Map<String, String[]> parameterMaps = request.getParameterMap();
        this.insertAccesslog(accesslog, parameterMaps);
    }
}
