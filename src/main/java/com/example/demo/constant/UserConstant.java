package com.example.demo.constant;

/**
 * Created by jake on 19-4-14.
 */
public enum UserConstant {
    INIT(0, "用户初始化"),
    NORMAL(1, "正常"),
    LOCKED(2, "用户被锁"),
    OUT_OF_DATE(3, "用户已过期");


    public int status;
    public String msg;

    UserConstant(int status, String msg) {
        this.status = status;
        this.msg = msg;
    }

    public int getStatusByMsg(String msg) {
        int status = -1;
        UserConstant[] enums = UserConstant.values();
        for (UserConstant e : enums) {
            if (e.msg.equals(msg)) {
                return e.status;
            }
        }
        return status;
    }

    public String getMsgByStatus(int status) {
        String msg = "状态未知";
        UserConstant[] enums = UserConstant.values();
        for (UserConstant e : enums) {
            if (e.status == status) {
                return e.msg;
            }
        }
        return msg;
    }
}
