package com.example.demo.dao.access;

import com.example.demo.entity.access.AccesslogDetail;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AccesslogDetailMapper {

    /**
     * 新增一条访问记录详情
     */
    public void insertAccesslogDetail(AccesslogDetail accesslogDetail);

    /**
     * 根据访问id 获取访问参数详情
     * @param accesslogid
     * @return
     */
    public List<AccesslogDetail> listAccesslodDetails(int accesslogid);
}
