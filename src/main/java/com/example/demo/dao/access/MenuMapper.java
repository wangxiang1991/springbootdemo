package com.example.demo.dao.access;

import com.example.demo.entity.access.Menu;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by jake on 19-4-14.
 */
@Mapper
public interface MenuMapper {

    /**
     * 插入菜单
     *
     * @param menu
     */
    public void insertMenu(Menu menu);

    /**
     * 更新菜单
     *
     * @param menu
     */
    public void updateMenu(Menu menu);

    /**
     * 根据id删除菜单
     *
     * @param id
     */
    @Delete("delete from menu where id=#{id}")
    public void deleteMenu(int id);

    /**
     * 根据id查找菜单
     *
     * @param id
     * @return
     */
    @Select("select * from menu where id=#{id}")
    public Menu findMenuById(int id);

    /**
     * 根据菜单名称查询菜单
     */
    @Select("select * from menu where name=#{name}")
    public Menu findMenuByName(String name);

    /**
     * 查找所有的菜单
     *
     * @return
     */
    @Select("select * from menu where isdelete != 1 order by sort asc")
    public List<Menu> listAllMenu();

    /**
     * 根据父节点找子菜单
     *
     * @param parentid
     * @return
     */
    @Select("select * from menu where parentid=#{parentid} and isdelete != 1 and isshow=1")
    public List<Menu> findChildMenus(String parentid);

    /**
     * 查找所有的一级菜单
     *
     * @return
     */
    @Select("select * from menu where parentid = '0' or parentid is null")
    public List<Menu> listTopMenus();

    /**
     * 删除所有的菜单
     */
    @Delete("delete from menu")
    public void deleteAll();
}
