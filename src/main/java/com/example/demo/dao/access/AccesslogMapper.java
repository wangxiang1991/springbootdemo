package com.example.demo.dao.access;

import com.example.demo.entity.access.Accesslog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AccesslogMapper {
    /**
     * 新增一条访问记录
     * @param accesslog
     */
    public int inserAccesslog(Accesslog accesslog);

    public List<Accesslog> listAccesslogs();
}
