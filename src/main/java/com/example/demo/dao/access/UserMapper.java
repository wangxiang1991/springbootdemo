package com.example.demo.dao.access;

import com.example.demo.entity.access.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface UserMapper {

    public int insertUser(User user);

    /**
     * 根据用户名查找用户
     * @param username
     * @return
     */
    @Select("select * from user where username=#{username}")
    public User findByUsername(String username);

    /**
     * 根据手机号查找用户
     */
    @Select("select * from user where mobilephone=#{phone}")
    public User findByMobilphone(String phone);

    /**
     * 根据email查找用户
     */
    @Select("select * from user where email=#{email}")
    public User findByEmail(String email);

    /**
     * 根据nickname查找用户
     */
    @Select("select * from user where nickname=#{nickname}")
    public User findByNickname(String nickname);

    /**
     * 根据用户名删除用户
     * @param username
     */
    @Delete("delete from user where username=#{username}")
    public void deleteUserByUsername(String username);

    /**
     * 更新用户属性
     */
    @Update("update user set status=#{status},username=#{username}," +
            "nickname=#{nickname},password=#{password},mobilephone=#{mobilephone}," +
            "email=#{email},address=#{address},credentialssalt=#{credentialssalt}," +
            "createdate=#{createdate},endDate=#{endDate} where id=#{id}")
    public void updateUser(User user);
}
