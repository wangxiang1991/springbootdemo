package com.example.demo.controller;

import com.example.demo.entity.access.Menu;
import com.example.demo.entity.tree.TreeNode;
import com.example.demo.service.access.IMenuService;
import com.example.demo.utils.MessageBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;
import java.util.List;
import java.util.Map;

@Controller
public class MenuController {
    private transient static final Logger LOGGER = LoggerFactory.getLogger(MenuController.class);

    @Autowired
    private IMenuService menuService;

    @RequestMapping("/user/menuTree/{parentId}")
    public String getMenuTree(@PathVariable Integer parentId, HttpServletRequest request) {
        List<Menu> menus = menuService.listMenuAsTree(parentId);
        request.setAttribute("menus", menus);
        request.setAttribute("parentId", parentId);
        return "system/menuTree";
    }

    @RequestMapping(value = "/user/menus", method = RequestMethod.GET)
    public String menu(HttpServletRequest request) {
        List<Menu> menus = menuService.listAllMenu();
        request.setAttribute("menus", menus);
        return "system/menuList";
    }

    @RequestMapping(value = "/user/menu/{id}")
    public String getMenu(@PathVariable Integer id, HttpServletRequest request) {
        Menu menu = menuService.findMenuById(id);
        request.setAttribute("menu", menu);
        return "system/menuForm";
    }


    @ResponseBody
    @RequestMapping(value = "/menu/delete/{id}")
    public Object delete(@PathVariable Integer id) {
        MessageBody result = menuService.deleteByid(id);
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/menu/update/{id}")
    public Object update(@PathVariable Integer id,HttpServletRequest request) {
        MessageBody result = menuService.updateMenu(id);
        return result;
    }

    @RequestMapping("/menu/tree/{parentid}")
    public Object getMenuTree(@PathVariable Integer parentid){
        TreeNode treeNode=new TreeNode();
        return treeNode;
    }
}
