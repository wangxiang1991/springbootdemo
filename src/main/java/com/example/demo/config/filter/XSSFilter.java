package com.example.demo.config.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class XSSFilter implements Filter {
    private transient static final Logger LOGGER=LoggerFactory.getLogger(XSSFilter.class);

    private static List<XSSRule> rules=new ArrayList<>();



    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        chain.doFilter(request,response);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // 从配置文件 里面加载所有的xssrules
    }

    @Override
    public void destroy() {

    }


    /**
     *  因为 filter无法改变 request 里面的参数  所以 写个请求的包装类
     */
    class XSSHttpServletRequestWrapper extends HttpServletRequestWrapper {
        public XSSHttpServletRequestWrapper(HttpServletRequest request) {
            super(request);
        }


    }
}
