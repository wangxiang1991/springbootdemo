package com.example.demo.config.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class XSSHttpServerletRequestWrapper extends HttpServletRequestWrapper {
    public XSSHttpServerletRequestWrapper(HttpServletRequest request) {
        super(request);
    }
}
